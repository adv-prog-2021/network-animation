import random
import matplotlib.pyplot as plt

class Node():
    def __init__(self, id):
        self.id = id
        self.list=[]
        self.counter=0

    def next(self, list_of_nodes):
        self.list = list_of_nodes

class Packet():
    def __init__(self, current_pos = None):
        self.current_pos = current_pos

    def forward(self):
        rand_node = random.choice(self.current_pos.list)
        self.current_pos = rand_node
        self.current_pos.counter += 1

n1 = Node(1)
n2 = Node(2)
n3 = Node(3)
n4 = Node(4)
n5 = Node(5)
n6 = Node(6)
n7 = Node(7)

n1.next([n2, n3, n4])
n2.next([n4, n5])
n3.next([n4])
n4.next([n5,n6])
n5.next([n7])
n6.next([n3])
n7.next([n4, n6])

# import ipdb; ipdb.set_trace()
p_list = []
for i in range(10):
    p_list.append(Packet(n1))

for i in range(100):
    for p in p_list:
        p.forward()

print(n2.counter)
c = range(1,8)
height = []
nodes_list = [n1, n2, n3, n4, n5, n6, n7]
for n in nodes_list:
    height.append(n.counter)

plt.bar(c, height, width=0.8)
plt.show()

