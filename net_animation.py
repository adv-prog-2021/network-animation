import pygame
from pygame.math import Vector2
import random 

class Node():
    def __init__(self, id, pos):
        self.id = id
        self.pos = pos
        self.list=[]
        self.counter=0

    def next(self, list_of_nodes):
        self.list = list_of_nodes

class Packet(pygame.sprite.Sprite):
    def __init__(self, current_node):
        super().__init__()
        self.image = pygame.image.load('digit.png').convert()
        self.rect = self.image.get_rect(center=current_node.pos)
        self.current_node = current_node
        self.velocity = Vector2(0, 0)
        self.max_vel = 5
        self.pos = Vector2(current_node.pos)
        self.random_choice()

    def update(self):
        # import ipdb, os; ipdb.set_trace()
        # A vector pointing from current position to target position
        steering = Vector2(self.next_node.pos) - Vector2(self.pos)
        distance = steering.length()  # Distance to the target.
        if distance <= 5: # If we get too close to the next node
            self.current_node = self.next_node
            self.random_choice()
        
        if distance != 0: # To avoid getting normalization errors when the length of the vector is zero
            steering.normalize_ip()
        self.velocity = steering * self.max_vel
        self.pos += self.velocity
        self.rect.center = self.pos

    def random_choice(self):
        self.next_node = random.choice(self.current_node.list)
       

def  main():
    screen = pygame.display.set_mode((800,600))
    clock = pygame.time.Clock()

    n1 = Node(1, (100,300))
    n2 = Node(2, (250,100))
    n3 = Node(3, (250,500))
    n4 = Node(4, (400,300))
    n5 = Node(5, (550,100))
    n6 = Node(6, (550,500))
    n7 = Node(7, (700,300))

    n1.next([n4])
    n2.next([n4, n5, n7])
    n3.next([n4, n7, n2, n1])
    n4.next([n1, n2, n3, n5,n6])
    n5.next([n1, n2, n3, n7])
    n6.next([n4, n5, n7])
    n7.next([n2, n3, n4, n6])
    
    nodes = [n1, n2, n3, n4, n5, n6, n7]
    # import ipdb, os; ipdb.set_trace()
    # all_packets = pygame.sprite.Group(Packet(n1.pos, n1))
    
    packet_list = []
    for i in range(10):
        p = Packet(n1)
        packet_list.append(p)
    # import ipdb; ipdb.set_trace() 

    EXIT = False
    while not EXIT:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                EXIT = True
        
        # all_packets.update()
        screen.fill((255, 255, 255))
        # all_packets.draw(screen)
        for node in nodes:
            pygame.draw.circle(screen, (0,0,0), node.pos, 10, 3)

        for pac in packet_list:
            # import ipdb, os; ipdb.set_trace()
            pac.update()
            screen.blit(pac.image,pac.pos)        
        pygame.display.update()
        clock.tick(30)

if __name__ == '__main__':
    pygame.init()
    main()
    pygame.quit()

