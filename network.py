#!/home/akomaty/anaconda3/envs/py37/bin/python

import networkx as nx 
import matplotlib.pyplot as plt 
import ipdb

G = nx.DiGraph() # Initialize an empty directional graph
G.add_nodes_from(range(1,26))
print(G.nodes())
G.add_edges_from([(1,2),(1,4),(1,5),(1,6),
                  (2,3),(2,10),(2,11),(2,12),
                  (3,10),(3,12),
                  (4,3),(4,19),(4,9),
                  (5,4),(5,11),(5,18),
                  (6,5),(6,16),
                  (7,5),(7,6),(7,9),(7,10),(7,16),(7,17),
                  (8,2),(8,5),(8,9),(8,12),(8,13),(8,20),
                  (9,3),(9,6),(9,10),(9,11),(9,15),(9,17),(9,18),
                  (10,4),(10,5),(10,6),(10,14),(10,16),(10,21),
                  (11,10),(11,12),(11,14),(11,15),
                  (12,13),(12,22),
                  (13,3),(13,4),(13,16),(13,21),
                  (14,21),
                  (15,12),(15,19),(15,21),
                  (16,14),(16,17),
                  (17,6),(17,19),
                  (18,10),(18,14),(18,15),(18,17),(18,20),
                  (19,18),
                  (20,3),(20,9),(20,15),(20,16),(20,17),(20,19),(20,21),
                  (21,11),(21,16),
                  (22,3),(22,15),(22,20),(22,21),
                  (23,14),(23,19),(23,20),(23,21),(23,22),
                  (24,12),(24,13),(24,14),(24,21),(24,22),
                  (25,16),(25,17),(25,18),(25,19),(25,20)
                ])
print(G.edges())

# ipdb.set_trace()
nx.draw(G, with_labels=True, font_weight='bold')
plt.show()

nx.draw_random(G, with_labels=True, font_weight='bold')
plt.show()

nx.draw_circular(G, with_labels=True, font_weight='bold')
plt.show()

nx.draw_spectral(G, with_labels=True, font_weight='bold')
plt.show()

